require 'open-uri'
require 'timeout'
require 'nokogiri'
require 'csv'
require 'yaml'

class TimeoutHandler
  def self.execute(options = {})
    raise "block required" unless block_given?

    retries = options[:retries] || 10

    begin
      Timeout::timeout(options[:timeout] || 30) do
        yield
      end

    rescue Timeout::Error, Errno::ECONNRESET => e
      retries -= 1
      if retries > 0
        Rails.logger.warn "caught #{e.class.name} executing block - will retry #{retries} more time(s).  (message = '#{e.message}')"
        sleep 0.5
        retry
      else
        raise
      end
    end
  end
end

class UriUtil
  def self.dom(url, options = {})
    if options[:type] == :xml
      Nokogiri::XML(get(url, options))
    else
      Nokogiri::HTML(get(url, options))
    end
  end

  private

  def self.get(url, options = {})
    TimeoutHandler.execute(options) { open(URI.escape(url)).read }
  end

  def self.params(url)
    hsh = {}
    unless url.empty?
      url[/\?(.+)/, 1].split('&').each do |param|
        kv = param.split('=')
        hsh[kv[0].to_sym] = kv.size > 1 ? kv[1] : ''
      end
    end
    hsh
  end
end

##########################################################################################

class Connections
  def initialize(csv_file)
    yaml_file = "#{csv_file[0...csv_file.rindex('.')]}.yaml"
    if File.exists?(yaml_file)
      puts "reading work data from #{yaml_file}"
      @records = YAML.load_file yaml_file

    else
      @records = []
      skip_first = true
      CSV.foreach(csv_file) do |row|
        if skip_first
          skip_first = false
          next
        end

        next if row[0].nil?

        @records << { :common_name => row[0],
                      :search_name => row[1],
                      :type => row[2].downcase.to_sym,
                      :institution => row[3]
        }
      end

      @records.sort_by! { |r| r[:search_name] }

      build_connections

      puts "writing work data to #{yaml_file}"
      File.open(yaml_file, "wb") do |f|
        f.write @records.to_yaml
      end
    end
  end

  def records
    @records
  end

  def neato_graph_data
    "graph G {\n#{build_neato_graph_data}\n}"
  end

  def circos_table_data
    build_circos_table_data.map { |row| row.map{ |c| c.gsub(/\s+/, '_') }.join("\t") }.join("\n")
  end

  private

  def build_circos_table_data
    names = @records.map{ |r| r[:search_name] }

    rows = []
    rows << ['data', names.size.times.map{ |x| "#{x + 1}" }].flatten
    rows << ['data', build_circos_colors].flatten
    rows << ['data', names].flatten  # header row

    @records.each do |r|
      arr = []
      arr << r[:search_name]
      names.each do |name|
        if r.key? :connections
          conn = r[:connections].select{ |c| c[:with] == name }
          if conn.any?
            arr << "#{conn[0][:count]}"
            next
          end
        end
        arr << '-'
      end
      rows << arr
    end

    rows
  end

  def build_circos_colors
    type_counts = {}
    @records.each do |r|
      if type_counts.key? r[:type]
        type_counts[r[:type]][:total] += 1
      else
        type_counts[r[:type]] = { :total => 1 }
      end
    end

    type_counts.each_value do |v|
      v[:count] = 0
    end

    colors = []
    @records.each do |r|
      tc = type_counts[r[:type]]
      colors << circos_color(r[:type], tc[:count], tc[:total])
      tc[:count] += 1
    end
    colors
  end

  BASE_COLOR = 100
  def circos_color(type, count, total)
    case type
      when :senior  then "#{BASE_COLOR + ((255 - BASE_COLOR) * (count.to_f / total.to_f)).to_i},0,0"
      when :junior  then "0,#{BASE_COLOR + ((255 - BASE_COLOR) * (count.to_f / total.to_f)).to_i},0"
      when :advisor then "0,0,#{BASE_COLOR + ((255 - BASE_COLOR) * (count.to_f / total.to_f)).to_i}"
      else               '0,0,0'
    end
  end

  def build_neato_graph_data
    node_map = {}
    @records.each_with_index do |r, index|
      node_map[r[:search_name]] = "n#{index}"
    end

    arr = []
    @records.each do |r|
      arr << "\t#{node_map[r[:search_name]]} [shape=#{neato_shape(r[:type])}, label=\"#{r[:common_name]}\"];"
    end

    @records.each do |r|
      if r.key? :connections
        r[:connections].each do |conn|
          arr << "\t#{node_map[r[:search_name]]} -- #{node_map[conn[:with]]} [label=\"#{conn[:count]}\"];"
        end
      end
    end
    arr << "\toverlap=scale;"
    arr.join "\n"
  end

  def neato_shape(type)
    case type
      when :senior  then 'doublecircle'
      when :junior  then 'circle'
      when :advisor then 'diamond'
      else               'ellipse'
    end
  end

  def build_connections
    puts "building connections ... this may take a while ..."
    hsh = {}
    @records.collect{ |r| r[:search_name] }.combination(2).each do |authors|
      puts "processing #{authors[0]} -> #{authors[1]}"
      count = num_connections(authors[0], authors[1])
      if count > 0
        hsh[authors[0]] = [] unless hsh.key?(authors[0])
        hsh[authors[0]] << { :with => authors[1], :count => count }
      end
    end

    @records.each do |r|
      r[:connections] = hsh[r[:search_name]] if hsh.key?(r[:search_name])
    end
    puts "done building connections."
  end

  def num_connections(author1, author2)
    dom = UriUtil.dom(search_url("#{author1.gsub(/\s+/, '+')}[AU]+#{author2.gsub(/\s+/, '+')}[AU]"), :type => :xml)
    dom.css('eSearchResult > Count').first.text.to_i
  end

  def search_url(term)
    "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term=#{term}"
  end
end

##########################################################################################


raise "Please specify a valid CSV file" unless $ARGV[0] && File.file?($ARGV[0]) && $ARGV[0].downcase.end_with?('.csv')

c = Connections.new $ARGV[0]

outfile = $ARGV[0][0...$ARGV[0].rindex('.')]

if $ARGV[1] == 'neato'
  dot_file = "#{outfile}.dot"
  File.open(dot_file, "wb") do |f|
    f.write c.neato_graph_data
  end
  puts "wrote output to #{dot_file}"

  neato = `which neato`.strip
  unless neato.empty?
    png_file = "#{outfile}.png"
    `#{neato} -Tpng #{dot_file} > #{png_file}`
    puts "neato graph written to #{png_file}"
  end

elsif $ARGV[1] == 'circos'
  tbl_file = "#{outfile}.tbl"
  File.open(tbl_file, "wb") do |f|
    f.write c.circos_table_data
    f.write "\n"
  end
  puts "wrote output to #{tbl_file}"

  if ENV['CIRCOS_HOME']
    script_path = File.dirname(__FILE__)
    circos_bin = "#{ENV['CIRCOS_HOME']}/bin"
    tableviewer_bin = "#{ENV['CIRCOS_HOME']}/tools/tableviewer/bin"

    if Dir.exists?(tableviewer_bin)
      ctbl_file = "#{outfile}.ctbl"
      `#{tableviewer_bin}/parse-table -conf #{script_path}/etc/parse-table.conf -file #{tbl_file} > #{ctbl_file}`
      `cat #{ctbl_file} | #{tableviewer_bin}/make-conf -dir #{script_path}/data`

      out_path = "#{script_path}/img"
      Dir.mkdir(out_path) unless Dir.exists?(out_path)
      `#{circos_bin}/circos -conf #{script_path}/etc/circos.conf -outputdir #{out_path}`

    else
      puts "warn : #{tableviewer_bin} does not exist!  not generating image."
    end
  else
    puts "warn : CIRCOS_HOME is not defined!  not generating image."
  end

else
  puts "No second-stage processing action specified.  Please rerun and specify second parameter 'neato', or 'circos'"
end


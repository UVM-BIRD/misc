# import.rb
#
# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
# 
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
######################################################################################
#
# This script simplifies the importation of undefined data into MySQL, Hive, and 
# Netezza databases.  It does this by analyzing each data file (e.g. a CSV formatted 
# representation of a single database table) to determineeach field's most accurate 
# data type, from which it can then automatically create andpopulate the tables.
# 
# Run the program without any arguments for usage instructions:
#
#     $ ruby ./import.rb
#
######################################################################################
#
# You may obtain the latest version of this script from the UVM-BIRD 'misc' repository
# hosted at Bitbucket.org:
#
#     https://bitbucket.org/UVM-BIRD/misc
#
######################################################################################

require 'csv'
require 'yaml'
require 'java'
require 'tmpdir'
require 'digest/md5'

######################################################################################
## class definitions

class DataError < Exception
end

class ParameterError < Exception
end

class IO
  def readline_nonblock
    buf = nil

    begin
      ch = self.read_nonblock(1)
      buf = ""
      until ch == "\n"
        buf << ch
        ch = self.read_nonblock(1)
      end
      buf << ch

    rescue Exception => e
      puts "readline_nonblock : caught #{e.class.name} - #{e.message}"
    end

    buf
  end
end

def assert(condition, fail_message)
  raise DataError, fail_message unless condition
end


def digest(obj)
  Digest::MD5.hexdigest(obj)
end

def exit_msg(message)
  puts message
  exit 0
end

def exit_error(message, params = {})
  puts "error : #{message}"
  if params[:error]
    puts "backtrace:"
    params[:error].backtrace.each() { |line| puts "  #{line}" }
  end
  exit -1
end

def execute_shell_command(cmd)
  $log.debug "executing command:\n#{cmd}"

  system(cmd)

  $?.to_i
end

def time_diff_ms(start, finish)
  ((finish.to_f - start.to_f) * 1000).to_i
end

def build_chunk_info(file_name, row_delimiter = "\n", skip_first_row = false)
  f = File.open file_name
  file_size = f.size

  begin
    # ensure that we create at least one chunk per core, with a minimum chunk size of 32 KB and a maximum chunk size of 64 MB
    available_cores = java.lang.Runtime.get_runtime.available_processors
    file_offset = skip_first_row ? f.readline.length : 0
    bytes_to_process = file_size - file_offset
    min_chunk_size = 2**15    # 32 kilobytes
    chunk_size = 64 * 2**20   # 64 megabytes
    chunks = (bytes_to_process.to_f / chunk_size).ceil

    if chunks < available_cores
      # would like to use all cores if possible, so long as chunks aren't stupidly small
      if (bytes_to_process / available_cores) < min_chunk_size
        chunk_size = min_chunk_size
        chunks = (bytes_to_process.to_f / chunk_size).ceil

      else
        chunks = available_cores
        chunk_size = bytes_to_process / chunks
      end
    end

    chunk_info = []
    chunk_info << { :start => file_offset }

    if chunks > 1
      # logically split the file into chunks, with each chunk being roughly equal in size, but ensuring that each
      # chunk contains complete records.  breaks between chunks must be made on valid record delimiters (newline chars)

      (chunks - 1).times do |x|
        chunk_offset = file_offset + ((x + 1) * chunk_size)

        f.seek(chunk_offset, IO::SEEK_SET)

        # step up through the next newline
        last_row_delimiter_offset = chunk_offset
        begin
          last_row_delimiter_offset += 1 while f.readchar != row_delimiter
        rescue EOFError
          $log.warn "EOF in file '#{file_name}' not preceded by row delimiter"
        end

        # set the ending offset for the previous chunk, and the starting offset for the next one
        chunk_info[chunk_info.size - 1][:end] = last_row_delimiter_offset
        chunk_info << { :start => last_row_delimiter_offset + 1 }
      end

    else
      chunk_size = file_size - file_offset
    end

  ensure
    f.close
  end

  # set the ending offset for the last chunk
  chunk_info[chunk_info.size - 1][:end] = file_size

  $log.info "file #{file_name} : size = #{file_size}, chunks = #{chunks}, chunk size = #{chunk_size}"
  $log.debug "chunk_info = #{chunk_info}"

  chunk_info
end

######################################################################################
## class definitions

class KeyValuePair
  attr_accessor :key, :value

  def initialize(key, value)
    @key = key
    @value = value
  end
end

class Logger
  NONE = KeyValuePair.new "NONE", 0
  ERROR = KeyValuePair.new "ERROR", 1
  WARN = KeyValuePair.new "WARN", 2
  INFO = KeyValuePair.new "INFO", 3
  DEBUG = KeyValuePair.new "DEBUG", 4

  def initialize(level_str = INFO.key)
    case level_str.upcase
      when NONE.key then @level = NONE
      when ERROR.key then @level = ERROR
      when WARN.key then @level = WARN
      when INFO.key then @level = INFO
      when DEBUG.key then @level = DEBUG
      else
        raise ParameterError, "invalid log level '#{level_str}'"
    end
  end

  def error?
    @level.value >= ERROR.value
  end

  def error(message)
    puts "error : #{message}" if error?
  end

  def warn?
    @level.value >= WARN.value
  end

  def warn(message)
    puts "warn : #{message}" if warn?
  end

  def info?
    @level.value >= INFO.value
  end

  def info(message)
    puts "info : #{message}" if info?
  end

  def debug?
    @level.value >= DEBUG.value
  end

  def debug(message)
    puts "debug : #{message}" if debug?
  end
end

class StemColumn # because it can become any type of Column - get it?
  attr_reader :name, :type, :length, :is_var_length, :before_decimal, :after_decimal, :min, :max, :nullable, :date_format, :date_delim, :bool_format

  def initialize(name)
    assert name =~ /^[A-Za-z0-9_]+$/, "invalid column name '#{name}'"
    @name = name
    @type = nil
    @length = nil
    @is_var_length = false
    @before_decimal = 0
    @after_decimal = 0
    @min = 0
    @max = 0
    @nullable = false
    @do_initial_bool_check = true
    @do_initial_date_check = true
    @was_delimited_date = false
    @date_delim = nil
    @date_format = nil
    @date_regex = nil
    @bool_format = nil
  end

  def consume(sc)
    @is_var_length = @is_var_length || sc.is_var_length || (@length != nil && sc.length != nil && @length != sc.length)
    @length = (@length == nil ? sc.length : [@length, sc.length].max) unless sc.length.nil?
    @before_decimal = [@before_decimal, sc.before_decimal].max
    @after_decimal = [@after_decimal, sc.after_decimal].max
    @min = [@min, sc.min].min
    @max = [@max, sc.max].max
    @nullable = @nullable || sc.nullable

    if @type.nil?
      @type = sc.type
      @date_format = sc.date_format
      @date_delim = sc.date_delim
      @bool_format = sc.bool_format

    elsif @type == :date
      unless sc.type == :date && @date_format == sc.date_format && @date_delim == sc.date_delim
        if sc.type == :date
          if @date_delim != sc.date_delim                   # (mm-dd-yyyy, mm/dd/yyyy), (mm-dd-yyyy, mmddyyyy)
            @type = :char

          elsif @date_delim == ''                           # yyyymmdd, mmddyyyy
            @type = :int if @date_format != sc.date_format

          elsif @date_format != sc.date_format              # yyyy-mm-dd vs mm-dd-yyyy
            @type = :char
          end

        elsif sc.type == :char
          @type = :char

        elsif sc.type == :int
          @type = @date_delim == '' ? :int : :char

        elsif sc.type == :numeric
          @type = @date_delim == '' ? :numeric : :char
        end
      end

    elsif @type == :bool
      @type = :char unless sc.type == :bool && @bool_format == sc.bool_format

    elsif @type == :numeric
      @type = :char unless sc.type == :int || sc.type == :numeric

    elsif @type == :int
      unless sc.type == :int
        @type = sc.type == :numeric ? :numeric : :char
      end
    end

  end

  def definition
    nullable_def = @nullable ? "" : " NOT NULL"

    case @type
      when :date then     "DATE(#{@date_format}, '#{@date_delim}')#{nullable_def}"
      when :bool then     "BOOL(#{@bool_format})#{nullable_def}"
      when :char then     "#{@is_var_length ? "VAR" : ""}CHAR(#{@length.to_s})#{nullable_def}"
      when :numeric then  "NUMERIC(#{precision}, #{scale})#{nullable_def}"
      when :int then      "INT#{bytes}#{nullable_def}"
      else                nil
    end
  end

  def precision
    @before_decimal + @after_decimal
  end

  def scale
    @after_decimal
  end

  def bytes
    if @min >= -128 && @max <= 127
      bytes = 1
    elsif @min >= -32768 && @max <= 32767
      bytes = 2
    elsif @min >= -2147483648 && @max <= 2147483647
      bytes = 4
    elsif @min >= -9223372036854775808 && @max <= 9223372036854775807
      bytes = 8
    else
      bytes = :too_big_for_int
    end
    bytes
  end

  def month?(m)
    m >= 1 && m <= 12
  end

  def day?(d)
    d >= 1 && d <= 31
  end

  def year?(y)
    y >= 1800 && y <= 2100
  end

  def set_min_max(value)
    @min = value if value < @min
    @max = value if value > @max
  end

  def require_value(value)
    if value.nil? || value.empty?
      @nullable = true unless @nullable
      return
    end

    if @length.nil?
      @length = value.length

    else
      @is_var_length = value.length != @length unless @is_var_length
      @length = value.length if value.length > @length
    end

    if @type == :date
      if @date_format == :YMD
        ymd = @date_regex.match(value)
        @type = nil if ymd.nil? || ! year?(ymd[1].to_i) || ! month?(ymd[2].to_i) || ! day?(ymd[3].to_i)

      else
        mdy = @date_regex.match(value)
        @type = nil if mdy.nil? || ! month?(mdy[1].to_i) || ! day?(mdy[2].to_i) || ! year?(mdy[3].to_i)
      end

      if @type == :date
        return

      else
        @type = nil
        @was_delimited_date = @date_delim.length > 0 # important as the delimiter is non-numeric, so new type must be char/varchar
      end

    elsif @do_initial_date_check
      @do_initial_date_check = false

      ymd = /^([0-9]{4})([^0-9])([0-9]{2})\2([0-9]{2})$/.match(value)
      if ymd != nil && year?(ymd[1].to_i) && month?(ymd[3].to_i) && day?(ymd[4].to_i)
        @date_format = :YMD
        @date_delim = ymd[2]
        @date_regex = Regexp.new("^([0-9]{4})#{Regexp.escape(@date_delim)}([0-9]{2})#{Regexp.escape(@date_delim)}([0-9]{2})$")

      else
        mdy = /^([0-9]{2})([^0-9])([0-9]{2})\2([0-9]{4})$/.match(value)
        if mdy != nil && month?(mdy[1].to_i) && day?(mdy[3].to_i) && year?(mdy[4].to_i)
          @date_format = :MDY
          @date_delim = mdy[2]
          @date_regex = Regexp.new("^([0-9]{2})#{Regexp.escape(@date_delim)}([0-9]{2})#{Regexp.escape(@date_delim)}([0-9]{4})$")

        elsif /^[0-9]{8}$/.match(value)
          set_min_max value.to_i

          if year?(value[0,4].to_i) && month?(value[4,2].to_i) && day?(value[6,2].to_i)
            @date_format = :YMD
            @date_delim = ''
            @date_regex = /^([0-9]{4})([0-9]{2})([0-9]{2})$/

          elsif month?(value[0,2].to_i) && day?(value[2,2].to_i) && year?(value[4,4].to_i)
            @date_format = :MDY
            @date_delim = ''
            @date_regex = /^([0-9]{2})([0-9]{2})([0-9]{4})$/
          end
        end
      end

      if @date_format != nil
        @type = :date
        return
      end
    end

    if @type == :bool
      v = value.downcase

      case @bool_format
        when :T_F then        matches = v == 't' || v == 'f'
        when :Y_N then        matches = v == 'y' || v == 'n'
        when :TRUE_FALSE then matches = v == 'true' || v == 'false'
        when :YES_NO then     matches = v == 'yes' || v == 'no'
        else                  matches = false
      end

      case matches
        when true then  return
        else            @type = nil
      end

    elsif @do_initial_bool_check
      @do_initial_bool_check = false

      case value.downcase
        when 't', 'f' then        @bool_format = :T_F
        when 'y', 'n' then        @bool_format = :Y_N
        when 'true', 'false' then @bool_format = :TRUE_FALSE
        when 'yes', 'no' then     @bool_format = :YES_NO
        else                      @bool_format = nil
      end

      if @bool_format != nil
        @type = :bool
        return
      end
    end

    if @type == :char ||                        # already designated char
        value =~ /[^-0-9.]/ ||                  # OR contains non-decimal +/- numeric characters
        value =~ /^0[0-9]+/ ||                  # OR is numeric, but number is left-padded with zeroes
        value =~ /\..*\./ ||                    # OR contains more than one decimal point
        value =~ /^.+-/ ||                      # OR contains a dash anywhere NOT at the beginning of the string
        @was_delimited_date                     # OR if this field used to contain a char-delimited date

      if @type != :char
        @type = :char
        @was_delimited_date = false
      end

    elsif @type == :numeric ||          # already designated numeric
        value =~ /^-?[0-9]+\.[0-9]+$/   # OR contains only numbers with one decimal point somewhere inside

      @type = :numeric unless @type == :numeric

      decimal_pos = value.index('.')
      bd = decimal_pos.nil? ? value.length : decimal_pos
      ad = decimal_pos.nil? ? 0 : value.length - decimal_pos - 1

      @before_decimal = bd if bd > @before_decimal
      @after_decimal = ad if ad > @after_decimal

    elsif value =~ /^-?[0-9]+$/   # an integer number
      @type = :int if @type.nil?

      set_min_max value.to_i

      @type = :numeric if @min < -9223372036854775808 || @max > 9223372036854775807
      @before_decimal = value.length if value.length > @before_decimal
    end
  end
end

class Column
  attr_reader :name, :allow_null

  def initialize(name, allow_null)
    assert name =~ /^[A-Za-z0-9_]+$/, "invalid column name '#{name}'"
    assert allow_null.is_a?(TrueClass) || allow_null.is_a?(FalseClass), "invalid value for allow_null '#{allow_null}'"

    @name = name
    @allow_null = allow_null
  end

  private

  def sql_definition(db_type, &block)
    sql = "#{@name} #{yield}"
    sql = sql + " NOT NULL" unless @allow_null
    sql
  end
end

class BoolColumn < Column
  VALID_FORMATS = [ :T_F, :Y_N, :TRUE_FALSE, :YES_NO ]

  def initialize(name, allow_null)
    super(name, allow_null)
  end

  def sql_definition(db_type)
    if db_type == :hive
      super { |sql| "BOOLEAN" }

    else
      super { |sql| "BOOL" }
    end
  end
end

class DateColumn < Column
  VALID_FORMATS = [ :YMD, :MDY ]

  def initialize(name, allow_null)
    super(name, allow_null)
  end

  def sql_definition(db_type)
    if db_type == :hive
      super { |sql| "TIMESTAMP" }

    else
      super { |sql| "DATE" }
    end
  end
end

class CharColumn < Column
  attr_reader :length, :is_variable

  def initialize(name, length, is_variable, allow_null)
    super(name, allow_null)

    assert length > 0 && length < 64000, "invalid length '#{length}'"
    assert is_variable.is_a?(TrueClass) || is_variable.is_a?(FalseClass), "invalid value for is_variable '#{is_variable}'"

    @length = length
    @is_variable = is_variable
  end

  def sql_definition(db_type)
    if db_type == :hive
      super { |sql| "STRING" }

    elsif db_type == :mysql && length > 255
      super do |sql|
        "TEXT"
      end

    else
      super do |sql|
        @is_variable ?
            "VARCHAR(#{@length})" :
            "CHAR(#{@length})"
      end
    end
  end
end

class IntegerColumn < Column
  VALID_SIZES = [ 1, 2, 4, 8 ]

  attr_reader :size

  def initialize(name, size, allow_null)
    super(name, allow_null)

    assert VALID_SIZES.include?(size), "invalid size '#{size}'"

    @size = size
  end

  def sql_definition(db_type)
    if db_type == :hive
      case @size
        when 1 then hive_type = "TINYINT"
        when 2 then hive_type = "SMALLINT"
        when 4 then hive_type = "INT"
        when 8 then hive_type = "BIGINT"
        else hive_type = nil
      end

      raise "invalid integer size: #{@size}" if hive_type.nil?

      super { |sql| hive_type }

    else
      super { |sql| "INT#{@size}" }
    end
  end
end

class NumericColumn < Column
  attr_reader :precision, :scale

  # precision is he number of digits in a number.  scale is the number of digits to
  # the right of the decimal point.  e.g., 123.45 has a precision of 5 and a scale of 2
  def initialize(name, precision, scale, allow_null)
    super(name, allow_null)

    assert precision > 0, "invalid precision '#{precision}'"
    assert scale >= 0 && scale <= precision, "invalid scale '#{scale}'"

    @precision = precision
    @scale = scale
  end

  def sql_definition(db_type)
    if db_type == :hive
      super { |sql| @precision <= 23 ? "FLOAT" : "DOUBLE" }

    else
      super { |sql| "NUMERIC(#{@precision}, #{@scale})" }
    end
  end
end

class Table
  attr_reader :name, :source_files, :date_format, :date_delim, :bool_format, :cols

  def initialize(name, source_files)
    assert name =~ /^[A-Za-z_]+$/, "invalid table name '#{name}'"

    @name = name
    @source_files = source_files
    @date_format = nil
    @date_delim = nil
    @bool_format = nil
    @cols = []
  end

  def create_sql(db_type)
    col_sql_definitions = []
    @cols.each { |c| col_sql_definitions << c.sql_definition(db_type) }
    "CREATE TABLE #{@name} (#{col_sql_definitions.join(', ')});"
  end

  def drop_sql(db_type)
    if db_type == :hive
      "DROP TABLE IF EXISTS #{@name};"

    else
      "DROP TABLE #{@name};"
    end
  end

  def save(data_dir)
    filename = "#{data_dir}/#{@name}.yml"
    $log.info "writing #{@name} table definition to #{filename}"
    File.open(filename, "w") { |f| f.write(self.to_yaml) }
  end

  def date_format=(format)
    assert format != nil && DateColumn::VALID_FORMATS.include?(format), "invalid date format '#{format}'"
    assert format == @date_format, "date format '#{format}' incompatible with existing value '#{@date_format}'" unless @date_format.nil?

    @date_format = format
  end

  def date_delim=(delim)
    assert delim != nil && (delim.length == 0 || delim.length == 1), "invalid date delimiter '#{delim}'"
    assert delim.eql?(@date_delim), "date delimiter '#{delim}' incompatible with existing value '#{@date_delim}'" unless @date_delim.nil?

    @date_delim = delim
  end

  def bool_format=(format)
    assert format != nil && BoolColumn::VALID_FORMATS.include?(format), "invalid boolean format '#{format}'"
    assert format == @bool_format, "boolean format '#{format}' incompatible with existing value '#{@bool_format}'" unless @bool_format.nil?

    @bool_format = format
  end
end

class DatabaseConnection
  attr_accessor :host, :database, :user, :pass

  def initialize(host, database, user, pass)
    assert host.length > 0, "invalid host '#{host}'"
    assert database.length > 0, "invalid database '#{database}'"

    @host = host
    @database = database
    @user = user
    @pass = pass
  end
end

class Database
  def self.create(type, conn)
    case type.to_sym
      when :mysql then    MySQL.new conn
      when :netezza then  Netezza.new conn
      when :hive then     Hive.new conn
      else
        raise ParameterError "invalid type '#{type}'"
    end
  end

  def drop_tables(table_defs)
    $log.info "dropping tables previously created ..."
    execute_shell_command drop_tables_command(table_defs)
  end

  def create_tables(table_defs)
    $log.info "(re)creating tables ..."
    result_code = execute_shell_command create_tables_command(table_defs)
    exit_error "failed to create tables : code #{result_code}" if result_code > 0
  end

  def populate_table(data_dir, table_defs, df_prefix, df_suffix, df_delim, df_quotes, df_encoding)
    $log.info "processing data files in '#{data_dir}' ..."
    $log.debug "df_prefix='#{df_prefix}', df_suffix='#{df_suffix}', df_delim='#{df_delim}', df_quotes='#{df_quotes}', df_encoding='#{df_encoding}'"

    log_folder = "#{data_dir}/log"
    Dir.mkdir(log_folder) unless File.exists?(log_folder)

    table_defs.each_value() do |table|
      table.source_files.each_with_index do |source_file, index|
        file_name = "#{data_dir}/#{source_file}"

        $log.debug("checking file '#{file_name}'")

        if File.exists? file_name
          cmd = populate_table_command file_name, table, df_delim, df_quotes, df_encoding, log_folder

          $log.debug "cmd = {#{cmd}}"
          $log.info "loading file '#{file_name}' -> table '#{table.name}'"
          start = Time.now
          result_code = execute_shell_command(cmd)

          if result_code == 0
            $log.info "finished loading data into table '#{table.name}'.  took #{time_diff_ms(start, Time.now)} ms."

            # todo : db.copy_permissions  # copies user and group permissions from #{database_name} to #{database_name}_NEXT
            # todo : db.cycle             # drops #{database_name}_LAST, renames #{database_name}_CURRENT to #{database_name}_LAST, and renames #{database_name}_NEXT to #{database_name}_CURRENT

          else
            $log.info "cmd =\n#{cmd}"
            exit_error "failed to populate table : code #{result_code}"
          end
        end
      end
    end
  end

  private

  def initialize(conn)
    @conn = conn
  end
end

class MySQL < Database
  def initialize(conn)
    super conn

    @mysql = `which mysql`.gsub(/\/{2,}/, '/').strip
    exit_error "mysql not found in path!" if @mysql.empty?
  end

  private

  def base_cmd
    cmd = "#{@mysql} -h'#{@conn.host}' -D'#{@conn.database}' -u'#{@conn.user}'"
    cmd << " -p'#{@conn.pass}'" unless @conn.pass.empty?
    cmd
  end

  def drop_tables_command(table_defs)
    cmd = "#{base_cmd} <<eof\n"
    table_defs.each_value() do |t|
      cmd << t.drop_sql(:mysql) + "\n"
    end
    cmd << "eof\n"
    cmd
  end

  def create_tables_command(table_defs)
    cmd = "#{base_cmd} <<eof\n"
    table_defs.each_value() do |t|
      cmd << t.create_sql(:mysql) + "\n"
    end
    cmd << "eof\n"
    cmd
  end

  def populate_table_command(input_file, table, df_delim, df_quotes, df_encoding, log_folder)
    case df_quotes.downcase.to_sym
      when :double then quotes = "\""
      when :single then quotes = "'"
      else quotes = ""
    end

    #cmd << " -dateStyle '#{table.date_format}'" unless table.date_format.nil?
    #cmd << " -dateDelim '#{table.date_delim}'" unless table.date_delim.nil?
    #cmd << " -boolStyle '#{table.bool_format}'" unless table.bool_format.nil?
    #cmd << " -df '#{input_file}' -outputDir '#{log_folder}' -ctrlChars -nullValue ''"

    case table.date_format
      when :YMD then date_format = "%Y#{table.date_delim}%m#{table.date_delim}%d"
      when :MDY then date_format = "%m#{table.date_delim}%d#{table.date_delim}%Y"
      else date_format = nil
    end

    col_defs = []
    col_vars = []
    table.cols.each do |col|
      if col.is_a?(DateColumn)
        col_defs << "@#{col.name}"
        col_vars << "#{col.name} = str_to_date(@#{col.name}, '#{date_format}')"
      else
        col_defs << "#{col.name}"
      end
    end

    format = File.open(input_file).readline.end_with?("\r\n") ? :windows : :unix

    cmd = "#{base_cmd} <<eof\n"
    cmd << "load data local infile '#{input_file}' into table #{table.name}\n"
    cmd << "fields terminated by '#{df_delim}' enclosed by '#{quotes}'\n"
    cmd << "lines terminated by '#{format == :windows ? '\r\n' : '\n' }'\n"
    cmd << "ignore 1 lines\n"
    cmd << "(#{col_defs.join(",\n ")})"
    if col_vars.size > 0
      cmd << "\nset #{col_vars.join(",\n ")}"
    end
    cmd << "\neof\n"
    cmd
  end
end

class Netezza < Database
  def initialize(conn)
    super conn

    @nzload = `which nzload`.gsub(/\/{2,}/, '/').strip
    exit_error "nzload not found in path!" if @nzload.empty?

    @nzsql = `which nzsql`.gsub(/\/{2,}/, '/').strip
    exit_error "nzsql not found in path!" if @nzsql.empty?
  end

  private

  def drop_tables_command(table_defs)
    cmd = "#{@nzsql} -h '#{@conn.host}' -d '#{@conn.database}' -u '#{@conn.user}' -pw '#{@conn.pass}' <<eof\n"
    table_defs.each_value() do |t|
      cmd << t.drop_sql(:netezza) + "\n"
    end
    cmd << "eof\n"
    cmd
  end

  def create_tables_command(table_defs)
    cmd = "#{@nzsql} -h '#{@conn.host}' -d '#{@conn.database}' -u '#{@conn.user}' -pw '#{@conn.pass}' <<eof\n"
    table_defs.each_value() do |t|
      cmd << t.create_sql(:netezza) + "\n"
    end
    cmd << "eof\n"
    cmd
  end

  def populate_table_command(input_file, table, df_delim, df_quotes, df_encoding, log_folder)
    cmd = "#{@nzload} -host '#{@conn.host}' -db '#{@conn.database}' -u '#{@conn.user}' -pw '#{@conn.pass}' -t '#{table.name}' -skipRows '1' -delim '#{df_delim}' -quotedValue '#{df_quotes}'"
    cmd << " -dateStyle '#{table.date_format}'" unless table.date_format.nil?
    cmd << " -dateDelim '#{table.date_delim}'" unless table.date_delim.nil?
    cmd << " -boolStyle '#{table.bool_format}'" unless table.bool_format.nil?
    cmd << " -df '#{input_file}' -outputDir '#{log_folder}' -ctrlChars -nullValue ''"
    cmd
  end
end

class Hive < Database
  def initialize(conn)
    super conn

    @hive = `which hive`.gsub(/\/{2,}/, '/').strip
    @hive = "#{ENV['HIVE_HOME']}/bin/hive" if @hive.empty? && ! ENV['HIVE_HOME'].nil? && ! ENV['HIVE_HOME'].empty?
    exit_error "hive not found in path and HIVE_HOME is not set!" if @hive.empty?
  end

  private

  def base_cmd
    cmd = "#{@hive}"
    cmd
  end

  def drop_tables_command(table_defs)
    cmd = "#{base_cmd} <<eof\n"
    cmd << "USE #{@conn.database};\n"
    table_defs.each_value() do |t|
      cmd << t.drop_sql(:hive) + "\n"
    end
    cmd << "eof\n"
    cmd
  end

  def create_tables_command(table_defs)
    cmd = "#{base_cmd} <<eof\n"
    cmd << "CREATE DATABASE IF NOT EXISTS #{@conn.database};\n"
    cmd << "USE #{@conn.database};\n"
    table_defs.each_value() do |t|
      cmd << t.create_sql(:hive) + "\n"
    end
    cmd << "eof\n"
    cmd
  end

  def populate_table_command(input_file, table, df_delim, df_quotes, df_encoding, log_folder)
    hive_input_file = convert_to_hive(input_file, table, df_delim, df_quotes, df_encoding)
    cmd = "#{base_cmd} -e \"load data local inpath '#{hive_input_file}' overwrite into table #{@conn.database}.#{table.name};\""
    cmd
  end

  def convert_to_hive(input_file, table, df_delim, df_quotes, df_encoding)
    hive_file = "#{input_file}.hive"
    return hive_file if File.exists? hive_file

    raise "invalid file '#{input_file}'" unless File.exists? input_file

    col_types = []
    table.cols.each do |col|
      case col
        when BoolColumn then    col_types << :boolean
        when DateColumn then    col_types << :date
        when CharColumn then    col_types << :string
        when IntegerColumn then col_types << :integer
        when NumericColumn then col_types << :numeric
        else
          raise "unhandled column class #{col.class.name}"
      end
    end

    config = { :field_delim => df_delim,
               :date_delim => table.date_delim,
               :date_format => table.date_format,
               :bool_format => table.bool_format,
               :quote_level => df_quotes.downcase.to_sym,
               :encoding => df_encoding,
               :col_types => col_types }

    mutex = Mutex.new
    available_cores = java.lang.Runtime.get_runtime.available_processors
    chunk_info_arr = build_chunk_info(input_file, "\n", true)
    chunk_file_arr = []
    tmpdir = Dir.mktmpdir('hive')
    pool = java.util.concurrent.Executors.newFixedThreadPool(available_cores)

    begin
      f = File.open(input_file, :mode => "r", :encoding => df_encoding)
      chunk_info_arr.each_with_index do |chunk_info, index|
        chunk_file = "#{tmpdir}/#{table.name}.#{index}.tmp"
        chunk_file_arr << chunk_file

        pool.execute do
          rows = nil
          mutex.synchronize {
            f.seek(chunk_info[:start], IO::SEEK_SET)
            rows = CSV.new f.read(chunk_info[:end] - chunk_info[:start] + 1), :col_sep => df_delim, :headers => false
          }

          convert_chunk(index, rows, chunk_file, config)
        end
      end

      pool.shutdown
      pool.awaitTermination(5, java.util.concurrent.TimeUnit::DAYS)

      puts

      chunk_file_arr.each do |chunk_file|
        raise "chunk file #{chunk_file} does not exist!" unless File.exists?(chunk_file)
      end

      File.open(hive_file, "a") do |target|
        chunk_file_arr.each do |chunk_file|
          $log.debug "writing #{chunk_file} to #{hive_file}"
          File.open(chunk_file) do |src|
            target.write(src.read(32768)) until src.eof?
          end
        end
      end

    ensure
      FileUtils.remove_entry_secure tmpdir
    end

    hive_file
  end

  def convert_chunk(id, rows, out_file_name, config)
    print "(start:#{id}) "
    progress_threshold = 10000
    row_count = 0

    out_file = File.new out_file_name, "w"
    begin
      rows.each do |row|
        begin
          hive_record = convert_row(row, config)
          out_file.write hive_record
          $log.debug "chunk_#{id}.row_#{row_count} : #{row} -> #{hive_record}"

          row_count += 1
          print '.' if row_count % progress_threshold == 0

        rescue Exception => e
          $log.error "caught #{e.class.name} processing row #{row_count} of chunk #{id} : #{e.message}"
          raise e
        end
      end

    ensure
      out_file.close
    end

    print "(end:#{id}) "
  end

  def convert_row(row, config)
    begin
      arr = []

      row.each_with_index do |val, index|
        case config[:quote_level]
          when :double then v = strip_leading_trailing(val, '"')
          when :single then v = strip_leading_trailing(val, '\'')
          else v = val
        end

        if v.nil? || v.empty?
          arr << '\N'

        else
          case config[:col_types][index]
            when :boolean then  arr << convert_boolean(v, config[:bool_format])
            when :date then     arr << convert_date(v, config[:date_format], config[:date_delim])
            else                arr << v
          end
        end
      end

      "#{arr.join(1.chr)}\n"

    rescue Exception => e
      $log.error "caught #{e.class.name} processing row {#{row}} : #{e.message}"
      raise e
    end
  end

  def strip_leading_trailing(val, char)
    val != nil && val.length >= 2 && val[0] == char && val[-1] == char ?
        val[1...-1] :
        val
  end

  def convert_date(v, date_format, date_delim)
    x = date_delim.length
    if date_format == :YMD
      y = v[0..3]
      m = v[4+x..5+x]
      d = v[6+2*x..7+2*x]

    elsif date_format == :MDY
      m = v[0..1]
      d = v[2+x..3+x]
      y = v[4+2*x..7+2*x]

    else
      raise "invalid date format '#{date_format}'"
    end

    "#{y}-#{m}-#{d} 00:00:00"
    #Time.new(y, m, d).to_i #* 1000
  end

  def convert_boolean(v, bool_format)
    v_sym = v.downcase.to_sym
    case bool_format
      when :T_F then        v_sym == :t     #? 'TRUE' : 'FALSE'
      when :Y_N then        v_sym == :y     #? 'TRUE' : 'FALSE'
      when :TRUE_FALSE then v_sym == :true  #? 'TRUE' : 'FALSE'
      when :YES_NO then     v_sym == :yes   #? 'TRUE' : 'FALSE'
      else
        raise "invalid boolean format '#{bool_format}'"
    end
  end
end

class Parameter
  attr_reader :key, :description, :prompt, :required
  attr_accessor :default
  attr_writer :value

  def initialize(key, description, prompt, hash = {})
    assert key.is_a?(Symbol) && key.to_s.length > 0 && key.to_s.length <= 15, "invalid key '#{key.to_s}'"
    assert description.length > 0, "invalid description '#{description}'"

    @key = key
    @description = description
    @prompt = prompt
    @required = hash.key?(:required) ? hash[:required] : true
    @default = hash.key?(:default) ? hash[:default] : nil
    @log = Logger.new DEFAULT_LOG_LEVEL
    @value = nil
  end

  def get_if_needed(default = @default)
    if @prompt != nil && (@value == nil || (@required && @value.empty?))
      if default.nil? || default.empty?
        @value = ''
        while @value.empty?
          print "#{@prompt.strip} "
          @value = STDIN.gets.strip

          break unless @required

          if @value.empty?
            @log.error "your response is required."
            @value = ''
          end
        end

      else
        print "#{@prompt.strip} [#{default}] "
        @value = STDIN.gets.strip
        @value = default if @value.empty?
      end
    end
  end

  def type
    "str"
  end

  def help
    str = "    -#{@key} <#{type}>".ljust(30, ' ') + @description
    str << " (default = '#{@default}')" unless @default.nil? || @default.empty?
    str
  end

  def populate(args = [])
    raise ParameterError, "missing value" if args.nil? || args.empty?
    arg = args.shift
    raise ParameterError, "missing value" if arg.start_with? '-'
    @value = arg
  end

  def value?
    ! @value.nil?
  end

  def value
    @value.nil? ? @default : @value
  end
end

class ToggleParameter < Parameter
  def initialize(key, description, hash = {})
    super key, description, nil, hash
  end

  def get_if_needed(default = nil)
  end

  def populate(args = [])
    @value = true
  end

  def type
    nil
  end
end

class PasswordParameter < Parameter
  def get_if_needed(default = nil)
    if @prompt != nil && (@value == nil || @value.empty?)
      @value = java.lang.String.new java.lang.System.console.read_password("#{@prompt.strip} ")
    end
  end

  def type
    "password"
  end
end

class FileParameter < Parameter
  def get_if_needed(default = @default)
    if @prompt != nil && (@value == nil || @value.empty?)
      @value = ''
      until File.file?(@value)
        super default
        @log.error("invalid file '#{@value}'") unless File.file?(@value)
      end
    end
  end

  def type
    "file"
  end
end

class DirParameter < Parameter
  def get_if_needed(default = @default)
    if @prompt != nil && (@value == nil || @value.empty?)
      @value = ''
      until File.directory?(@value)
        super default
        unless File.directory? @value
          @log.error("invalid path '#{@value}'")
          @value = ''
        end
      end
    end
  end

  def type
    "dir"
  end
end

class LogParameter < Parameter
  def initialize(key, description, default)
    super key, description, nil, default
  end

  def type
    "level"
  end
end

class ImportConfig
  VALID_TYPES = %w(mysql netezza hive)
  VALID_QUOTED_VALUES = %w(DOUBLE SINGLE NO)
  VALID_ENCODINGS = %w(ISO-8859-1 UTF-8)

  attr_accessor :type, :h, :d, :df_prefix, :df_suffix, :df_delim, :df_quotes, :df_encoding

  def initialize(params = {})
    assert(VALID_TYPES.include?(params[:type].downcase), "invalid type '#{params[:type]}'") if params.key?(:type)
    assert(params[:h] =~ /^[A-Za-z0-9\.]+$/, "invalid host '#{params[:h]}'") if params.key?(:h)
    assert(params[:d] =~ /^[A-Za-z][A-Za-z0-9_]+$/, "invalid database '#{params[:d]}'") if params.key?(:d)
    assert(params[:df_delim].length == 1, "invalid datafile delimiter '#{params[:df_delim]}'") if params.key?(:df_delim)
    assert(VALID_QUOTED_VALUES.include?(params[:df_quotes].upcase), "invalid quoted-value '#{params[:df_quotes]}'") if params.key?(:df_quotes)
    assert(VALID_ENCODINGS.include?(params[:df_encoding].upcase), "invalid encoding '#{params[:df_encoding]}'") if params.key?(:df_encoding)

    @type = params[:type].downcase
    @h = params[:h]
    @d = params[:d]
    @df_prefix = params[:df_prefix]
    @df_suffix = params[:df_suffix]
    @df_delim = params[:df_delim]
    @df_quotes = params[:df_quotes].upcase
    @df_encoding = params[:df_encoding].upcase
  end
end

######################################################################################
## main methods

def load_config(data_dir)
  filename = "#{data_dir}/.import/config.yml"
  if File.exists?(filename)
    config = YAML.load(File.open(filename))
    $log.debug "found existing config file '#{filename}'"
  else
    config = nil
  end
  config
end

def save_config(data_dir, config)
  unless config.nil?
    folder = "#{data_dir}/.import"
    Dir.mkdir(folder) unless File.exists?(folder)
    filename = "#{folder}/config.yml"
    $log.debug "writing config to '#{filename}'"
    File.open(filename, "w") { |f| f.write(config.to_yaml) }
  end
end

def load_table_defs(data_dir)
  table_hash = Hash.new
  Dir.glob("#{data_dir}/.import/table_defs/*.yml") do |filename|
    table = YAML.load(File.open(filename))
    $log.debug "found existing table definitions for table '#{table.name}'"
    table_hash[table.name] = table
  end
  table_hash
end

def save_table_def(data_dir, table)
  unless table.nil?
    folder = "#{data_dir}/.import/table_defs"
    Dir.mkdir(folder) unless File.exists?(folder)
    filename = "#{folder}/#{table.name}.yml"
    $log.debug "writing #{table.name} table definition to '#{filename}'"
    File.open(filename, "w") { |f| f.write(table.to_yaml) }
  end
end

def generate_table_defs(data_dir, df_prefix, df_suffix, df_delim, df_encoding)
  $log.info "generating table definitions from raw data files in '#{data_dir}' ..."
  $log.debug "df_prefix='#{df_prefix}', df_suffix='#{df_suffix}', df_delim='#{df_delim}', df_encoding='#{df_encoding}'"

  table_defs = load_table_defs data_dir
  start = Time.now
  files_processed = 0
  debug = $log.debug?

  table_data = {}
  regex = Regexp.new "^#{df_prefix}([A-Za-z0-9_]+)#{df_suffix}$"
  files = []
  Dir.foreach(data_dir) do |file|
    files << file if regex.match(file)
  end

  files.sort.each do |file|
    input_file = "#{data_dir}/#{file}"
    table_name = regex.match(file)[1]

    $log.debug '-- begin headers --'
    headers = []
    File.open(input_file, :encoding => df_encoding) do |f|
      begin
        CSV.parse_line(f.readline.strip, :col_sep => df_delim, :headers => false).each_with_index do |header, index|
          clean_header = header.gsub(/\(.+\)/, '')          # get rid of stuff in parentheses
                               .gsub(/[^A-Za-z0-9]/, ' ')   # convert invalid characters to spaces
                               .strip                       # remove leading and trailing spaces
                               .gsub(/\s+/, '_')            # convert spaces to single underscore

          $log.debug " - col #{index} : header='#{header}', clean_header='#{clean_header}'"
          headers << clean_header
        end
      rescue Exception => e
        $log.error "caught #{e.class.name} processing file #{input_file} : #{e.message}"
        raise e
      end
    end
    $log.debug '-- end headers --'

    key = digest(headers.join(';'))

    if table_data.key? key
      found = false
      table_data[key].each do |hsh|
        if table_name.start_with? hsh[:table_name]
          found = true
          hsh[:files] << file
          break
        end
      end

      table_data[key] << { :table_name => table_name, :headers => headers, :files => [file] } unless found

    else
      table_data[key] = [ { :table_name => table_name, :headers => headers, :files => [file] } ]
    end
  end

  table_data_arr = []
  table_data.each_value do |arr|
    table_data_arr.concat arr
  end

  table_data_arr.each do |data|
    table_name = data[:table_name]
    headers = data[:headers]

    unless table_defs.key?(table_name)
      table = Table.new table_name, data[:files]
      chunk_cols_arr = []
      row_count = 0
      file_start = Time.now
      mutex = Mutex.new
      available_cores = java.lang.Runtime.get_runtime.available_processors

##############################################

      data[:files].each do |file|
        $log.info "processing table '#{table_name}' (file '#{file}') - "

        input_file = "#{data_dir}/#{file}"
        chunk_info_arr = build_chunk_info(input_file, "\n", true)
        pool = java.util.concurrent.Executors.newFixedThreadPool(available_cores)

        begin
          f = File.open(input_file, :mode => "r", :encoding => df_encoding)
          chunk_info_arr.each_with_index do |chunk_info, index|
            pool.execute do
              rows = nil
              mutex.synchronize {
                f.seek(chunk_info[:start], IO::SEEK_SET)
                rows = CSV.new f.read(chunk_info[:end] - chunk_info[:start] + 1), :col_sep => df_delim, :headers => false
              }

              h = analyze_chunk index, rows, headers.map{ |header| StemColumn.new(header) }

              mutex.synchronize {
                chunk_cols_arr << h[:cols]
                row_count += h[:row_count]
              }
            end
          end

          pool.shutdown
          pool.awaitTermination(5, java.util.concurrent.TimeUnit::DAYS)

        ensure
          f.close if defined?(f) && f.is_a?(File)
        end
      end

##############################################

      cols = headers.map { |header| StemColumn.new(header) }

      chunk_cols_arr.each do |chunk_cols|
        chunk_cols.each_with_index do |col, index|
          if debug
            orig_desc = cols[index].definition
            cols[index].consume col
            new_desc = cols[index].definition
            $log.debug "generate_table_defs : #{cols[index].name} is now \"#{new_desc}\"" if orig_desc != new_desc

          else
            cols[index].consume col
          end
        end
      end

      puts

      duration = time_diff_ms(file_start, Time.now)
      $log.info "finished processing #{row_count} records from table '#{table.name}'.  took #{duration}ms (avg #{(duration / row_count.to_f).round(3)} ms / row)"

      cols.each do |col|
        case col.type
          when :bool
            table.cols << BoolColumn.new(col.name, col.nullable)
            begin
              table.bool_format = col.bool_format
            rescue DataError => e
              $log.warn "caught #{e.class.name} processing column '#{table_name}.#{col.name}' : #{e.message}"
            end

          when :date
            table.cols << DateColumn.new(col.name, col.nullable)
            begin
              table.date_format = col.date_format
            rescue DataError => e
              $log.warn "caught #{e.class.name} processing column '#{table_name}.#{col.name}' : #{e.message}"
            end

            begin
              table.date_delim = col.date_delim
            rescue DataError => e
              $log.warn "caught #{e.class.name} processing column '#{table_name}.#{col.name}' : #{e.message}"
            end

          when :char then table.cols << CharColumn.new(col.name, col.length, col.is_var_length, col.nullable)
          when :int then table.cols << IntegerColumn.new(col.name, col.bytes, col.nullable)
          when :numeric then table.cols << NumericColumn.new(col.name, col.precision, col.scale, col.nullable)
          else
            $log.warn "unable to determine data type for column (#{col.name}) - using VARCHAR(1) -"
            table.cols << CharColumn.new(col.name, 1, true, true)
        end
      end

      save_table_def data_dir, table
      table_defs[table.name] = table
      files_processed += 1
    end
  end

  if files_processed > 0
    $log.info "finished generating #{files_processed} table definitions.  took #{time_diff_ms(start, Time.now)} ms."

  elsif table_defs.size > 0
    $log.info "found no remaining table definitions to process."

  else
    $log.warn "no files processed and no table definitions found!  possible configuration issue?"
  end

  table_defs
end

def analyze_chunk(id, rows, cols)
  print "(start:#{id}) "
  debug = $log.debug?
  progress_threshold = 10000
  row_count = 0

  rows.each do |row|
    col_count = 0
    row.each do |value|
      if debug
        orig_desc = cols[col_count].definition
        cols[col_count].require_value value
        new_desc = cols[col_count].definition
        $log.debug "chunk_#{id}.row_#{row_count} : #{cols[col_count].name} is now \"#{new_desc}\" with value \"#{value}\"" if orig_desc != new_desc

      else
        cols[col_count].require_value value
      end

      col_count += 1
    end

    row_count += 1
    print '.' if row_count % progress_threshold == 0
  end

  print "(end:#{id}) "

  { :cols => cols,
    :row_count => row_count }
end


######################################################################################
## setup : validate files, directories, and required programs are available

DEFAULT_LOG_LEVEL = Logger::INFO.key

params = Array.new
params << DirParameter.new(:source, "A directory that contains source data files", "Specify source data directory:")
params << Parameter.new(:type, "Database type", "Specify 'netezza', 'hive', or 'mysql':")
params << Parameter.new(:h, "Database host / IP address", "Specify database host / IP address:")
params << Parameter.new(:d, "Database name", "Specify database name:")
params << Parameter.new(:u, "Database username", "Username:")
params << PasswordParameter.new(:p, "Password for specified database user", "Password:")
params << Parameter.new(:df_prefix, "Data filename prefix before table name", "Specify data filename prefix before table name:", :required => false)
params << Parameter.new(:df_suffix, "Data filename suffix after table name", "Specify data filename suffix after table name:", :required => false)
params << Parameter.new(:df_delim, "Data file field delimiter", "Specify data file field delimiter:")
params << Parameter.new(:df_quotes, "Data file field quote level", "Specify data file field quote-level (#{ImportConfig::VALID_QUOTED_VALUES.join(', ')}):", :default => ImportConfig::VALID_QUOTED_VALUES[0])
params << Parameter.new(:df_encoding, "Data file encoding", "Specify data file field encoding (#{ImportConfig::VALID_ENCODINGS.join(', ')}):", :default => ImportConfig::VALID_ENCODINGS[0])
params << LogParameter.new(:log, "Logging level", :default => DEFAULT_LOG_LEVEL)

p = Hash.new
params.each { |param| p[param.key] = param }

if ARGV.empty?
  puts '''Usage : import.rb <action> [options]
  Actions:
    analyze     Create table definitions by analyzing raw table data
    create      Create (or recreate) tables based on table definitions
    load        Loads source data into database
    configure   Reconfigure options, such as database, host, etc.

  Options:'''

  params.each { |p| puts p.help }
  puts

  exit 0
end

action = ARGV.shift.downcase

while ARGV.size > 0
  begin
    key = ARGV.shift
    if key.start_with? '-'
      k = key.slice(1..-1).to_sym
      if p.key? k
        p[k].populate ARGV
      else
        raise ParameterError, "invalid token"
      end
    end

  rescue ParameterError => e
    exit_error "token '#{key}' : #{e.message}"
  end
end

$log = Logger.new(p[:log].value)

p[:source].get_if_needed
p[:d].default = p[:source].value[/([^\/]+)\/?$/, 1].upcase

if action == "configure"
  config = nil
  old_config = load_config p[:source].value
  if old_config != nil
    p[:type].default = old_config.type
    p[:h].default = old_config.h
    p[:d].default = old_config.d
    p[:df_prefix].default = old_config.df_prefix
    p[:df_suffix].default = old_config.df_suffix
    p[:df_delim].default = old_config.df_delim
    p[:df_quotes].default = old_config.df_quotes
    p[:df_encoding].default = old_config.df_encoding
  end

else
  config = load_config p[:source].value
end

if config.nil?
  p[:type].get_if_needed
  p[:h].get_if_needed 'localhost'
  p[:d].get_if_needed
  p[:df_prefix].get_if_needed
  p[:df_suffix].get_if_needed
  p[:df_delim].get_if_needed
  p[:df_quotes].get_if_needed
  p[:df_encoding].get_if_needed

  config = ImportConfig.new :h => p[:h].value, :d => p[:d].value, :df_prefix => p[:df_prefix].value,
                      :df_suffix => p[:df_suffix].value, :df_delim => p[:df_delim].value,
                      :df_quotes => p[:df_quotes].value, :df_encoding => p[:df_encoding].value,
                      :type => p[:type].value

  save_config p[:source].value, config

else
  p[:type].value = config.type
  p[:h].value = config.h
  p[:d].value = config.d
  p[:df_prefix].value = config.df_prefix
  p[:df_suffix].value = config.df_suffix
  p[:df_delim].value = config.df_delim
  p[:df_quotes].value = config.df_quotes
  p[:df_encoding].value = config.df_encoding
end

if action == "configure"
  $log.info "Configuration updated."

elsif action == "analyze"
  p[:df_prefix].get_if_needed
  p[:df_suffix].get_if_needed
  p[:df_delim].get_if_needed
  p[:df_encoding].get_if_needed

  generate_table_defs p[:source].value, p[:df_prefix].value, p[:df_suffix].value, p[:df_delim].value, p[:df_encoding].value

elsif action == "create" || action == "load"
  table_defs = load_table_defs p[:source].value
  exit_error "couldn't find table definitions!  please run with action 'analyze' first and try again." if table_defs.empty?

  p[:type].get_if_needed
  p[:h].get_if_needed
  p[:d].get_if_needed

  # hive localhost config has user / pass configured in /usr/share/hive/conf/hive-site.xml
  unless p[:type].value.to_sym == :hive && %w(localhost 127.0.0.1).include?(p[:h].value.downcase)
    p[:u].get_if_needed
    p[:p].get_if_needed
  end

  db = Database.create p[:type].value,
                       DatabaseConnection.new(p[:h].value, p[:d].value, p[:u].value, p[:p].value)

  if action == "create"
    db.drop_tables table_defs

    # todo : db.create_database # creates a new database with _NEXT suffix

    db.create_tables table_defs

  else
    p[:df_prefix].get_if_needed
    p[:df_suffix].get_if_needed
    p[:df_delim].get_if_needed
    p[:df_encoding].get_if_needed

    db.populate_table p[:source].value, table_defs, p[:df_prefix].value, p[:df_suffix].value, p[:df_delim].value, p[:df_quotes].value, p[:df_encoding].value
  end

else
  exit_error "invalid action '#{action}'"
end

$log.info "done."
